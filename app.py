from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort
import os
from sqlalchemy.orm import sessionmaker
from tabledef import *
import json
from pprint import pprint

engine = create_engine('sqlite:///tutorial.db', echo=True)
 
app = Flask(__name__)

data = json.load(open('hotel.json'))
#json_pretty = json.dumps(data, sort_keys=True, indent=4)

#pprint(data)
hotel_names = [li['name_en'] for li in data]
#print(hotel_names)
# hotel_list = []
# for x in data['name_en']:
#     hotel_list.append(x['name_en']) 
#pprint(json_pretty)
 
@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return render_template('list.html', hotel_names=hotel_names, data= data)
        # pprint(data)
        # return "Hello Boss!  <a href='/logout'>Logout</a>"
 
@app.route('/login', methods=['POST'])
def do_admin_login():
 
    POST_USERNAME = str(request.form['username'])
    POST_PASSWORD = str(request.form['password'])
 
    Session = sessionmaker(bind=engine)
    s = Session()
    query = s.query(User).filter(User.username.in_([POST_USERNAME]), User.password.in_([POST_PASSWORD]) )
    result = query.first()
    if result:
        session['logged_in'] = True
    else:
        flash('wrong password!')
    return home()
 
@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()

@app.route('/badlapur', methods=['GET', 'POST'])
def badlapur():
    return render_template('badlapur.html', hotel_names=hotel_names, data=data)

@app.route('/booking', methods=['GET', 'POST'])
def booking():
    return render_template('booking.html', hotel_names=hotel_names, data=data)
 
if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True)